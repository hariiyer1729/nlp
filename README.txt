An NLP-driven content structuring system. This project is after the objective of
converting raw data into a format that can be used in multimodal representation
use-cases. A few ways of representing knowledge can be in the form of:

-- Graph nodes/Ontology
-- Application-supported output format (eg. Definition snippets/Google weather)
-- Audio (eg. e-learning applications)
-- Statistical data
-- Visual format (eg. images, color-codes)

Taking it step-by-step, we have started with commonly spoken language. Once this
is attained, the system aims at going further ahead for jargon-based data that
would help make it to the organizational level. Such applications include stuff
like ATC communication, business correspondence, and multilingual instances.

Once this model is up, a myriad of vernacular implementations can be addressed
that are currently missing out on the tribal knowledge data resources, which can
significantly humanize the machine's data interpretation routine. Those sectors
include:

-- Audio/visual sentiment analysis (Currently under consideration)
-- Efficient Email data interpretation (Business/Personalized use-cases)
-- Search engine optimization
-- Consumer feedback analysis
-- and endlessly more

Under development, the system is being built to attain all of this.

Hari Iyer.
