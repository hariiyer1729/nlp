#####################################################
#Writen by Hari Iyer				    #	
#Module text_analysis for ontology creation	    #
#Structuring data chunks into a knowledge graph     #
#Data input file taken for reference only           #
#####################################################

import nltk
import re
from nltk.tokenize import word_tokenize, sent_tokenize

file = open("data.txt", "r")
file_data =  file.read()
tokenized_file_data = word_tokenize(""+file_data)
token_pos_pair = nltk.pos_tag(tokenized_file_data)
verb_set = []
for i in token_pos_pair:
	fullstring = i[1]
	if "VB" in fullstring:
		verb_set.append(i)
#print verb_set	
	
from pprint import pprint
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet, movie_reviews
from nltk.classify import NaiveBayesClassifier
import random
"""
#POS tags

CC	coordinating conjunction
CD	cardinal digit
DT	determiner
EX	existential there (like: 'there is' ... think of it like 'there exists')
FW	foreign word
IN	preposition/subordinating conjunction
JJ	adjective	'big'
JJR	adjective, comparative	'bigger'
JJS	adjective, superlative	'biggest'
LS	list marker	1)
MD	modal	could, will
NN	noun, singular 'desk'
NNS	noun plural	'desks'
NNP	proper noun, singular	'Harrison'
NNPS	proper noun, plural	'Americans'
PDT	predeterminer	'all the kids'
POS	possessive ending	parent's
PRP	personal pronoun	I, he, she
PRP$	possessive pronoun	my, his, hers
RB	adverb	very, silently,
RBR	adverb, comparative	better
RBS	adverb, superlative	best
RP	particle	give up
TO	to	go 'to' the store.
UH	interjection	errrrrrrrm
VB	verb, base form	take
VBD	verb, past tense	took
VBG	verb, gerund/present participle	taking
VBN	verb, past participle	taken
VBP	verb, sing. present, non-3d	take
VBZ	verb, 3rd person sing. present	takes
WDT	wh-determiner	which
WP	wh-pronoun	who, what
WP$	possessive wh-pronoun	whose
WRB	wh-abverb	where, when
"""

#Stemming (Avoid this, lemmatization is way better)


porter_stemmer = PorterStemmer()
stemmable_words = ["fly", "flying", "flyer", "flyable"]
for w in stemmable_words:
	print(porter_stemmer.stem(w))

stem_sentence = "Flyers fly flyable planes."
stemmable_words = word_tokenize(stem_sentence)
for w in stemmable_words:
	print(porter_stemmer.stem(w))

#Word_Tokenization

sample = "It is a bright sunny day."
words = word_tokenize(sample)
print (sent_tokenize(sample))
print "Tokenized Sample:"
print words

#StopWords removal

stop_words = set(stopwords.words("english"))

#sweet_sample for holding just useful terms for processing
sweet_sample = []
for w in words:
	if w not in stop_words:
		sweet_sample.append(w) 

print "\nSweet sample:"
print sweet_sample

#Parts_of_speech

sample_text = "According to a principle of aerodynamics called Bernoulli's law, fast-moving air is at lower pressure than slow-moving air, so the pressure above the wing is lower than the pressure below, and this creates the lift that powers the plane upward."
custom_tokenizer = PunktSentenceTokenizer(sample_text)
tokenized = custom_tokenizer.tokenize(sample_text)

def process_content():
    try:
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            print(tagged)
		
    except Exception as e:
        print(str(e))

process_content()


#chunking

for i in tokenized_file_data:
            tagged = nltk.pos_tag(i)
chunk = r"Chunk: {<NN.?>+}"
chunkParser = nltk.RegexpParser(chunk)
chunk_result = chunkParser.parse(token_pos_pair)
chunk_result.draw()

#named entity recognition

namedEntityBinary = nltk.ne_chunk(tagged, binary=True)
namedEntity = nltk.ne_chunk(tagged)
namedEntityBinary.draw()
namedEntity.draw()


#Lemmatization (Meaningful stemming)
#lemmatizer.lemmatize(token, pos='v/n/a etc') The default pos is N, but if lemmaitzer is 
#expected to interpret differently, then it has to be specified.


lemmatizer = WordNetLemmatizer()
for i in tokenized:
	print lemmatizer.lemmatize(i)


#WordNet capabilities

words = word_tokenize(sample_text)
for token in words:
	for i in wordnet.synsets(""+token):
		print "-------------------------------------------------------------------------------------------------\nSynset: " +i.name()+"\nDefinition: "+i.definition()+"\nSynonyms:"
		for handle in i.lemmas():
			print "\t"+handle.name()
			if handle.antonyms():
				print "Antonyms:"
				print "\t"+handle.antonyms()[0].name()


#Semantic similarity

term1 = wordnet.synset('aircraft.n.01')
term2 = wordnet.synset('aeroplane.n.01')

print (term1.wup_similarity(term2))

term1 = wordnet.synset('cell.n.01')
term2 = wordnet.synset('chair.n.01')

print (term1.wup_similarity(term2))

term1 = wordnet.synset('tree.n.01')
term2 = wordnet.synset('bucket.n.01')

print (term1.wup_similarity(term2))



#Training dataset for videos/movies

collection = []
for category in movie_reviews.categories():
	for fileid in movie_reviews.fileids(category):
		collection.append((list(movie_reviews.words(fileid)), category))


random.shuffle(collection)  #To change data(training -> testing). 


all_words = []
for word in movie_reviews.words():
    all_words.append(word.lower())

all_words = nltk.FreqDist(all_words) #This is lot of data, needs to be compressed.
#print(all_words.most_common(10))
#print(all_words["hilarious"])

#For compressing, we select the topmost elements in the frequency distribution stack
consideration_cases = list(all_words.keys())[:1000]

#Routine for comparing consideration cases and universal set of collections
def analyzer(document):
	words = set(document)
	existence = {}
	for word in words:
		existence = str(word)+" "+str({word in consideration_cases}) 
	return existence

#Building list on the comparison results of above operation
featureset = []
for (rev, category) in collection:
	featureset.append((analyzer(rev), category))  
	#Calling function by sending data from "collection". Word data is consideration_cases, which is beforehand in scope.
pprint(featureset)


