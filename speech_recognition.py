import subprocess
import sys
import os
import re

file_name = os.path.splitext("airbus.mp4")[0]+".wav"
transcript = os.path.splitext("airbus.mp4")[0]
command_mp4ToWav = "ffmpeg -i airbus.mp4 -ac 1 -ar 16000 -vn "+file_name
subprocess.call(command_mp4ToWav, shell=True)
#command_wavToFlac = "flac "+file_name
#subprocess.call(command_wavToFlac, shell=True)
mono_er = "sox file_name -c 1 ili.wav avg -b"
subprocess.call(mono_er, shell=True)
transcription = "pocketsphinx_continuous -infile "+file_name+" > "+transcript
subprocess.call(transcription, shell=True)
#command_file_get_contents = "cat "+os.path.splitext("airbus.mp4")[0]
#subprocess.call(command_file_get_contents, shell=True)
with open(""+transcript, 'r') as content_file:
    content = content_file.read()
#print content
cleaned_transcript = re.sub('0[0-9]*:', ' ', ''+content, flags=re.IGNORECASE)
cleaned_transcript = cleaned_transcript.replace('\n', '')
cleaned_transcript = cleaned_transcript.replace('  ', '||') + "||"
print "Transcript of video:"
print cleaned_transcript




